;; Exwm Emacs X Windows Manageer  
(add-to-list 'load-path "~/.emacs.d/xelb/")
(add-to-list 'load-path "~/.emacs.d/elpa/exwm-0.24/")
(require 'exwm)
(require 'exwm-config)
(exwm-config-default)
(exwm-enable)
(setq exwm-workspace-minibuffer-position (quote bottom))
(setq exwm-workspace-number 10)
;;
;; Atajos de exwm para lanzar aplicaiones externas.
;;
;; Min
(exwm-input-set-key (kbd "s-m") (lambda ()
(interactive)
(let  ((comando "min --no-sandbox")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-m") 0) exwm-input-prefix-keys)
;;
;; Xterm
(exwm-input-set-key (kbd "s-x") (lambda ()
(interactive)
(let  ((comando "xterm")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-x") 0) exwm-input-prefix-keys)
;;
;; Mocp
(exwm-input-set-key (kbd "s-n") (lambda ()
(interactive)
(let  ((comando "xterm -e mocp")) 
  (start-process-shell-command comando nil comando))))

(push (elt (kbd "s-n") 0) exwm-input-prefix-keys)
;;
;; Alsamixer
(exwm-input-set-key (kbd "s-a") (lambda ()
(interactive)
(let  ((comando "xterm -e alsamixer")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-a") 0) exwm-input-prefix-keys)
;;
;; klavaro
(exwm-input-set-key (kbd "s-k") (lambda ()
(interactive)
(let  ((comando "klavaro")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-k") 0) exwm-input-prefix-keys)
;;
;; chromium
(exwm-input-set-key (kbd "s-e") (lambda ()
(interactive)
(let  ((comando "chromium")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-e") 0) exwm-input-prefix-keys)
;;
;; chrome
(exwm-input-set-key (kbd "s-g") (lambda ()
(interactive)
(let  ((comando "google-chrome")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-g") 0) exwm-input-prefix-keys)
;;
;; postman
(exwm-input-set-key (kbd "s-o") (lambda ()
(interactive)
(let  ((comando "postman")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-o") 0) exwm-input-prefix-keys)
;;
;; telegram
(exwm-input-set-key (kbd "s-t") (lambda ()
(interactive)
(let  ((comando "telegram-desktop")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-t") 0) exwm-input-prefix-keys)
;;
;; transmission
(exwm-input-set-key (kbd "s-r") (lambda ()
(interactive)
(let  ((comando "transmission-gtk")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-r") 0) exwm-input-prefix-keys)
;;
;; libreoffice
(exwm-input-set-key (kbd "s-l") (lambda ()
(interactive)
(let  ((comando "libreoffice")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-l") 0) exwm-input-prefix-keys)
;;
;; brave-browser
(exwm-input-set-key (kbd "s-b") (lambda ()
(interactive)
(let  ((comando "brave-browser")) 
(start-process-shell-command comando nil comando))))
(push (elt (kbd "s-b") 0) exwm-input-prefix-keys)
