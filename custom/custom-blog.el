;; Org-static-blog
;;
(setq org-static-blog-publish-title "EmacsFAQ")
(setq org-static-blog-publish-url "https://martiyo.github.io/")
(setq org-static-blog-publish-directory "~/projects/blog/")
(setq org-static-blog-posts-directory "~/projects/blog/posts/")
(setq org-static-blog-drafts-directory "~/projects/blog/drafts/")
(setq org-static-blog-use-preview t) 
(setq org-static-blog-enable-tags t)
(setq org-export-with-toc nil)
(setq org-export-with-section-numbers nil)

;; This header is inserted into the <head> section of every page:
;;   (you will need to create the style sheet at
;;    ~/projects/blog/static/style.css
;;    and the favicon at
;;    ~/projects/blog/static/favicon.ico)
(setq org-static-blog-page-header
"<meta name=\"author\" content=\"martiyo\">
<meta name=\"referrer\" content=\"no-referrer\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
<link href= \"static/style.css\" rel=\"stylesheet\" type=\"text/css\" />
<link rel=\"icon\" href=\"static/favicon.ico\">
")

;; This preamble is inserted at the beginning of the <body> of every page:
;;   This particular HTML creates a <div> with a simple linked headline
(setq org-static-blog-page-preamble
"<div class=\"header\">
<ul>
<li><a href=\"https://martiyo.github.io/\">/home</a></li>
<li><a href=\"https://martiyo.github.io/about\">/about</a></li>
<li><a href=\"https://martiyo.github.io/contact\">/contact</a></li>
</ul>
 <h1>El universo perdido de GNU/Emacs</h1>
 <blockquote>Este blog es una farsa son solo resumenes, traducciónes y cosas que encuentro por ahí.</blockquote>
<form >
<input type=\"text\" placeholder=\"Search...\" />
</form>
<br/>
</div>")

(setq org-static-blog-page-content
"<div class=\"content\">
 <div class=\"post-date\">		
</div>
</div>")

;; This postamble is inserted at the end of the <body> of every page:
;;   This particular HTML creates a <div> with a link to the archive page
;;   and a licensing stub.
(setq org-static-blog-page-postamble
"<div id=\"archive\">
  <a href=\"https://martiyo.github.io/archive.html\">Other posts</a>
</div>
<footer></footer>
")
