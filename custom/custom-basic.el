;;
;; Configuración básica
;;
;; Tamaño de la fuente
(set-face-attribute 'default nil :height 90)
;;
;; Cambiar tamaño de la fuente con C-+ y C--
(global-set-key [C-kp-add] 'text-scale-increase)
(global-set-key [C-kp-subtract] 'text-scale-decrease)
;;
;; Omitir mensaje de inicio
(setq inhibit-startup-message t)
;;
;; No mostrar la barra del menú
(menu-bar-mode -1)					
;;; Numeros de linea
;;global-linum-mode t
;; Mostrar numeros de linea conf f6
(global-set-key (kbd "<f6>") 'linum-mode)
;;
;;; Añadir un espacio entre el número de linea y el texto de la
;;; linea al lanzar M-x linum-mode (ver números de linea)
(setq linum-format "%d ")
;;
;; No hacer copias de seguredad ni crear archivos #auto-save#
(setq make-backup-files nil)
;(setq backup-inhibited t)
(setq auto-save-default nil)
;(setq delete-auto-save-files nil)
;(setq auto-save-mode nil)
;; No crear lockfiles
(setq create-lockfiles nil)
;;
;;; Reemplazar "yes" y "no" por "y" y "n"
(fset 'yes-or-no-p 'y-or-n-p)
;;
;;; Mover a la papelera al borrar archivos y directorios:
(setq delete-by-moving-to-trash t
trash-directory "~/.local/share/Trash/files")
;;
;; Guardar la sessión al cerrar emacs y restaurarla
;; al arrancar-la de nuevo. Cero (0) para desactivar:
(desktop-save-mode 0)
;;
;;
;; Eww navegador por defecto
(setq browse-url-browser-function 'eww-browse-url)
;; adaptar el texto al buffer eww
 (setq visual-mode-line t) 
;;
;;
;; Resaltar parentesis llaves y corchetes
(show-paren-mode t)
;;
;; Theme
;(use-package flatland-theme
;  :config
;  (custom-theme-set-faces 'flatland
;   '(show-paren-match ((t (:background "dark gray" :foreground "black" :weight bold))))
;   '(show-paren-mismatch ((t (:background "firebrick" :foreground "orange" :weight bold))))))
(load-theme 'solarized-dark t)
;;
;;
;; Cambiar tamaño ventana de buffer
(use-package buffer-move
 :config
 (global-set-key (kbd "<C-up>") 'shrink-window)
 (global-set-key (kbd "<C-down>") 'enlarge-window)
 (global-set-key (kbd "<C-left>") 'shrink-window-horizontally)
 (global-set-key (kbd "<C-right>") 'enlarge-window-horizontally))
;;
;;
;; Elimina texto seleccionado luego de pegar algo en su lugar.
(delete-selection-mode 1)
;;
