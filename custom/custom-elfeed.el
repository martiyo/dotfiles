;;
;; Elfeed
;;
(use-package elfeed
  :ensure t
  :custom
  ;;filtro de busqueda prederminado
  (elfeed-search-filter "@1-week-ago +unread")
  :config
  (global-set-key [f7] 'elfeed)
  (global-set-key [f8] 'elfeed-update)
  (global-set-key [f5] 'eww)

  (setq elfeed-feeds   
   '(("https://notxor.nueva-actitud.org/rss.xml" emacs)
     ("https://ondahostil.wordpress.com/feed/" emacs)
     ("http://emacsrocks.com/atom.xml" emacs)
     ("http://planet.emacs-es.org/feed" emacs)
     ("http://planet.emacs-es.org/atom.xml" emacs)
     ("http://blog.desdelinux.net/feed" linux)
     ("http://ugeek.github.io/feed")
     ("http://elatareao.es/feed")
     ("https://elblogdelazaro.gitlab.io/feed")
     ("http://quijotelibre.com/feed/")
     ("https://lapipaplena.wordpress.com/category/emacs/feed/" emacs)
     ("https://surdelsur.com/es/feed/")
     ("https://www.linuxito.com/?format=feed&type=rss")
     ("http://pragmaticemacs.com/feed/")
     ("https://gnutas.juanmanuelmacias.com/rss.xml" emacs)
     ("https://hijosdeinit.gitlab.io/")
     )
   )
)

;; (add-to-list 'load-path "~/.emacs.d/elpa/elfeed-20201220.1359")

;; ;:filtro de busqueda prederminado
;; (setq-default elfeed-search-filter "@1-week-ago +unread")
;; (global-set-key [f7] 'elfeed)
;; (global-set-key [f8] 'elfeed-update)
;; (global-set-key [f5] 'eww)

;; (setq elfeed-feeds   
;;       '(("https://notxor.nueva-actitud.org/rss.xml" emacs)
;; 	("https://ondahostil.wordpress.com/feed/" emacs)
;; 	("http://emacsrocks.com/atom.xml" emacs)
;; 	("http://planet.emacs-es.org/feed" emacs)
;; 	("http://planet.emacs-es.org/atom.xml" emacs)
;; 	("http://blog.desdelinux.net/feed" linux)
;; 	("http://ugeek.github.io/feed")
;; 	("http://elatareao.es/feed")
;; 	("https://elblogdelazaro.gitlab.io/feed")
;; 	("http://quijotelibre.com/feed/")
;; 	("https://lapipaplena.wordpress.com/category/emacs/feed/" emacs)
;; 	("https://surdelsur.com/es/feed/")
;; 	("https://www.linuxito.com/?format=feed&type=rss")
;; 	("http://pragmaticemacs.com/feed/")
;; 	)
;; )
