;;
;; Dashboard
(use-package dashboard
  :ensure t
  :custom
  (dashboard-banner-logo-title (format "Welcome to GNU/Emacs" user-full-name))
  (dashboard-startup-banner "~/storage/img/puño_mini.png")
  (dashboard-items '((recents . 4)
		     (projects . 4)
	 	     (bookmarks . 4)
  		     (agenda . 5)
		     
		     ))
  :config
  
  (setq dashboard-set-heading-icons t
	dashboard-set-file-icons t
	dashboard-set-init-info t
	dashboard-set-navigator t)
  
  (setq dashboard-navigator-buttons
	`((
	   (,(when (display-graphic-p)
	       (all-the-icons-octicon "home" :height 1.1 :v-adjust 0.0))
	    "FAQ" "El universo perdido de GNU/Emacs"
	    (lambda (&rest _) (browse-url "https://martiyo.github.io/")))
	   
	   (,(when (display-graphic-p)
	       (all-the-icons-material "home" :height 1.35 :v-adjust -0.24))
	    "Post directory" "Directorio post de org-static-blog"
	    (lambda (&rest _) (dired "~/projects/blog/posts/")))
	   
	   (,(when (display-graphic-p)
	       (all-the-icons-octicon "tools" :height 1.0 :v-adjust 0.0))
	    "Configuración" "Archivo de configuración de emacs"
	    (lambda (&rest _) (find-file (expand-file-name  "~/.emacs.d/init.el"))))
	   
	   (,(when (display-graphic-p)
	       (all-the-icons-octicon "list-ordered" :height 1.0 :v-adjust 0.0))
	    "TODO" "Abrir myTodo"
	    (lambda (&rest _) (find-file (expand-file-name  "~/org/todolist.org"))))
	   
	   (,(when (display-graphic-p)
	       (all-the-icons-octicon "calendar" :height 1.0 :v-adjust 0.0))
	    "Agenda" "Agenda personal"
	    (lambda (&rest _)
	      (interactive)
	      (if (get-buffer "*Org Agenda*")
		  (progn
		    (switch-to-buffer-other-window "*Org Agenda*")
		    (kill-buffer "*Org Agenda*")
		    (org-agenda-list))
		(split-window-right)
		(org-agenda-list))))
	   
	   )))
  :hook
  (after-init . dashboard-setup-startup-hook))
(global-set-key [f10] 'open-dashboard) ;F10 para ir al Dashboard

;;(setq dashboard-week-agenda t)
;;(setq dashboard-org-agenda-categories '("Tasks" "Appointments"))

