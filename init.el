;;
;;(setq gnutls-algorithm-priority "NORMAL: -VERS-TLS1.3")
;;
;; Repositorios
(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))
(package-initialize)
;;
;;
;; Use-package
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)

(setq use-package-always-ensure t)
;;
;;
;; Exwm
(setq custom-exwm "~/.emacs.d/custom/custom-exwm.el")
(load-file custom-exwm)
;;
;; Org-static-blog
(setq custom-blog "~/.emacs.d/custom/custom-blog.el")
(load-file custom-blog)
;;
;; Configuración basica
(setq custom-basic "~/.emacs.d/custom/custom-basic.el")
(load-file custom-basic)
;;
;; Dashboard
(setq custom-dashboard "~/.emacs.d/custom/custom-dashboard.el")
(load-file custom-dashboard)
;;
;; Ivy Councel y Swiper
(setq custom-ivy "~/.emacs.d/custom/custom-ivy.el")
(load-file custom-ivy)
;;
;; Elfeed
(setq custom-elfeed "~/.emacs.d/custom/custom-elfeed.el")
(load-file custom-elfeed)
;;
;; Agenda
(setq custom-agenda "~/.emacs.d/custom/custom-agenda.el")
(load-file custom-agenda)
;;
;; auto-complete
(add-to-list 'load-path "~/.emacs.d/auto-complete-20201011.1341/")
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/auto-complete-20201041/dict")
(ac-config-default)
(add-to-list 'ac-modes 'web-mode)
;;
;;
;; Org-mode
(use-package org
    :delight "Θ"
    :config
    (setq org-hide-emphasis-markers t))
;;
(font-lock-add-keywords 'org-mode
                        '(("^ +\\([-*]\\) "
                           (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))
;; Mejorando el especto de los bullets
(use-package org-bullets
:ensure t
:config
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))
;; Adaptando el texto a la ventana
(add-hook 'org-mode-hook 'visual-line-mode)
;;
;; Autocompletado para Org-mode
(use-package org-ac
  :ensure t
  :config
  (org-ac/config-default))
;;
;; Fin org
;;
;; Pdf-tools
 (use-package pdf-tools
   :ensure t
   :load-path "~/.emacs.d/elpa/pdf-tools-20220103.308"
   :magic ("%PDF" . pdf-view-mode)
   :config
   (pdf-tools-install :no-query)
   (add-hook 'pdf-tools-enabled-hook 'pdf-view-midnight-minor-mode)
   )
;; ;;
;;
;; NeoTree
 (use-package neotree
   :ensure t
   :load-path "~/.emacs.d/elpa/neotree-20200324.1946"
   :config
   (setq neo-theme 'icons)
   (global-set-key [f11] 'neotree-toggle)
   (setq neo-smart-open t)
   (setq projectile-switch-project-action 'neotree-projectile-action))
;; 
;;
;; Projectile
 (use-package projectile
   :ensure t
   :config
   (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
   (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
   (projectile-mode +1))
;;
;;
;; Web-mode
(use-package web-mode
  :ensure t
  :mode (("\\.js\\'" . web-mode)
         ("\\.jsx\\'" . web-mode)
         ("\\.ts\\'" . web-mode)
         ("\\.tsx\\'" . web-mode)
         ("\\.html\\'" . web-mode)
         ("\\.vue\\'" . web-mode)
	 ("\\.json\\'" . web-mode)
	 ("\\.hbs\\'" . web-mode)
	 ("\\.php\\'" . web-mode)
	 ("\\.css\\'" . web-mode)
	 ("\\.ejs\\'" . web-mode))
  
  
  :commands web-mode
  :config
   ;;resaltar comienzo y final de un tag
  (setq web-mode-enable-current-column-highlight t)
  (setq web-mode-enable-current-element-highlight t)
  ;;
  ;; Emmet
  (add-to-list 'load-path "~/.emacs.d/elpa/emmet-mode-20210820.1124")
  (require 'emmet-mode)
  (add-hook 'web-mode-hook  'emmet-mode)
  ;;
  ;; resaltar sintaxis jsx
  (setq web-mode-content-types-alist '(("jsx" . "\\.js[x]?\\'")))
  )
;;
;;
;; Emms
(add-to-list 'load-path "~/.emacs.d/emms/")
(require 'emms-setup)
(emms-standard)
;; ;;(emms-default-players)
(setq-default
    emms-player-list '(emms-player-mplayer))
   ;;  emms-player-mpv-debug t)
;; directorio predeterminado de musica
(setq emms-source-file-default-directory "~/storage/webDav/musica/")
;;escuchar radio
(global-set-key (kbd "C-c r") 'emms-play-m3u-playlist)
;;deshabilitar info track en mode-line
(emms-mode-line 0)
;;; Atajos para controlar el volumen
(global-set-key [f2] 'emms-volume-mode-plus)
(global-set-key [f1] 'emms-volume-mode-minus)
;;(global-set-key (kbd "C-c o") 'emms-volume-mode-plus)
;;Reproducir enlaces multimedia
;; (defun mi-multimedia-url-emms ()
;;   (interactive)
;;   (let ((url (get-text-property (point) 'shr-url)))
;;     (emms-play-url (format "%s" url))))

;; (with-eval-after-load 'eww
;;   (define-key eww-mode-map (kbd "m") 'mi-multimedia-url-emms))
;;
;;
;; Prettier
(add-to-list 'load-path "~/.emacs.d/elpa/prettier-js-20180109.726")
(require 'prettier-js)
(add-hook 'js2-mode-hook 'prettier-js-mode)
(add-hook 'web-mode-hook 'prettier-js-mode)
;;
;;
;; Aweshell
(add-to-list 'load-path "~/.emacs.d/aweshell")
(require 'aweshell)
(global-set-key (kbd "\C-x n") 'aweshell-new)
(global-set-key (kbd "\C-x x") 'aweshell-next)
(global-set-key (kbd "\C-x p") 'aweshell-prev)
(global-set-key (kbd "\C-l") 'aweshell-clear-buffer)
(global-set-key (kbd "\C-c b") 'aweshell-switch-buffer)
(global-set-key (kbd "\C-c o") 'aweshell-dedicated-open)
(global-set-key (kbd "\C-c t") 'aweshell-dedicated-toggle)
(global-set-key (kbd "\C-c c") 'aweshell-dedicated-close)
;;
;; Lanzar aweshell al inicio
;;(add-hook 'emacs-startup-hook 
;;(lambda ()
;;            (cd default-directory)
;;            (aweshell-new)))
;; Y ya que esta abrir myTodo.org
;;(find-file (expand-file-name "~/storage/webDav/org/myTodo.org" default-directory))
;;
;;
;; Dired
;;
(defun dired-mode-setup ()
  "hook para que dired-mode oculte propietario y permisos de archivo"
  (dired-hide-details-mode 1))
(add-hook 'dired-mode-hook 'dired-mode-setup)
;;
;;
(defun external-app ()
  "Abrir archivo con apps externas."
  (interactive)
  (let* ((file (dired-get-filename nil t)))
    (call-process "xdg-open" nil 0 nil file)))
;;; Atajo para abrir archivos con apps externas con f9.
(global-set-key [f9] 'external-app)
;;
;;
(defun dired-dotfiles-toggle ()
  "Mostrar/esconder archivos ocultos"
  (interactive)
  (when (equal major-mode 'dired-mode)
    (if (or (not (boundp 'dired-dotfiles-show-p)) dired-dotfiles-show-p)
	(progn
	  (set (make-local-variable 'dired-dotfiles-show-p) nil)
	  (message "h")
	  (dired-mark-files-regexp "^\.")
	  (dired-do-kill-lines))
      (progn (revert-buffer)
	     (set (make-local-variable 'dired-dotfiles-show-p) t)))))
;; Atajo para esconder/mostrar archivos ocultos con C-c w:
(global-set-key (kbd "C-c w") 'dired-dotfiles-toggle)
;;
;; Desde cualquier directorio de dired ir al home con (Shift+f1)
;; (global-set-key (kbd "S-[f1]")
;; 		(lambda()
;; 		  (interactive)
;; 		  (dired "~/")))
;; 
;;  Abrir directorio con Enter (subdirectorio) y ^ (directorio padre) en un mismo buffer.
 (define-key dired-mode-map (kbd "RET") 'dired-find-alternate-file) ; 
 (define-key dired-mode-map (kbd "^") (lambda () (interactive) (find-alternate-file "..")))
;; 
;;  En un archivo abrir el directorio de dicho archivo con C-c C-j
(require 'dired-x)
;; ;;
;; All the icons para Dired
(add-to-list 'load-path "~/.emacs.d/elpa/all-the-icons-dired-20200403.1018")
(load "all-the-icons-dired.el")
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;;
(put 'dired-find-alternate-file 'disabled nil)
;;
;;
;; Archivos recientes
(recentf-mode 1)
(setq recentf-max-menu-items 10)
(setq recentf-max-saved-items 10)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)
;;
;;
;; Google translate
(use-package google-translate
  :ensure t
  
:config
(when (and (string-match "0.12.0"
(google-translate-version))
             (>= (time-to-seconds)
		 (time-to-seconds
                  (encode-time 0 0 0 23 9 2018))))
    (defun google-translate--get-b-d1 ()
      ;; TKK='427110.1469889687'
      (list 427110 1469889687)))
(custom-set-variables
'(google-translate-default-source-language "en")
'(google-translate-default-target-language "es"))
)
;;
;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#26292c" "#ff4a52" "#40b83e" "#f6f080" "#afc4db" "#dc8cc3" "#93e0e3" "#f8f8f8"])
 '(eww-download-directory "~/storage/descargas")
 '(fci-rule-color "#202325")
 '(google-translate-default-source-language "en")
 '(google-translate-default-target-language "es")
 '(package-selected-packages
   '(pdf-tools shroud pass solarized-theme emmet-mode ivy-hydra magit ivy-posframe elfeed eshell-prompt-extras web-mode use-package switch-window smex projectile prettier-js prettier org-static-blog org-bullets org-ac neotree js2-mode google-translate flx flatland-theme exwm dashboard counsel company buffer-move all-the-icons-dired))
 '(vc-annotate-background "#1f2124")
 '(vc-annotate-color-map
   '((20 . "#ff0000")
     (40 . "#ff4a52")
     (60 . "#f6aa11")
     (80 . "#f1e94b")
     (100 . "#f5f080")
     (120 . "#f6f080")
     (140 . "#41a83e")
     (160 . "#40b83e")
     (180 . "#b6d877")
     (200 . "#b7d877")
     (220 . "#b8d977")
     (240 . "#b9d977")
     (260 . "#93e0e3")
     (280 . "#72aaca")
     (300 . "#8996a8")
     (320 . "#afc4db")
     (340 . "#cfe2f2")
     (360 . "#dc8cc3")))
 '(vc-annotate-very-old-color "#dc8cc3"))
 '(google-translate-default-source-language "en")
 '(google-translate-default-target-language "es")
 '(neo-theme (quote icons))
;;
;; 

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
